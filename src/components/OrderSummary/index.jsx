import React from 'react';
import styles from './index.module.scss';

const index = ({itemsCount, subTotal, tax}) => {
  return (
    <div className={styles.OrderSummary}>
      <h2>Order Summary</h2>
      <div className={styles.field}>
        <div>Subtotal ({itemsCount} items)</div>
        <div>{subTotal}</div>
      </div>
      <div className={styles.field}>
        <div>Tax (1.23%)</div>
        <div>{tax}</div>
      </div>
      <div className={styles.field}>
        <div>Shipping fee</div>
        <div>500</div>
      </div>
      <div className={styles.total}>
        <div>Total</div>
        <div>{subTotal + tax + 500}</div>
      </div>
      <button>Pay now</button>
    </div>
  );
}

export default index
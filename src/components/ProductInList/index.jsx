import React from "react";
import PriceDisplay from "../PriceDisplay";
import styles from "./index.module.scss";

// this is the each product component that gets displayed within the map inside products page

const ProductInList = ({ data, id, handleAdd }) => {
  const isOutOfStock = data.availability === 0;
  return (
    <div className={styles.ProductInList}>
      <img className={styles.productImage} src={data.imgURL} alt="" />
      <div className={styles.productMake}>{data.make}</div>
      <div className={styles.productName}>{data.name}</div>
      
      {/* // components have to be separated from the tiniest level */}
      <PriceDisplay price={data.price} />
      <div className={styles.overlay} />

      {isOutOfStock ? (
        <div className={styles.outOfStock}>
          <div>Out of stock</div>
        </div>
      ) : (
        <button className={styles.addToCart} onClick={() => handleAdd(data.id)}>
          Add to cart
        </button>
      )}
    </div>
  );
};

export default ProductInList;

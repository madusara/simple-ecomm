import React from 'react';
import styles from './index.module.scss';

const PriceDisplay = ({price}) => {
  return (
    <div className={styles.productPrice} >₹ {price}</div>
  )
}

export default PriceDisplay
import React from "react";
import { Modal } from "reactstrap";

// this modal is used to prompt the user to confirm the deletion of an item in the cart

const DeleteItemModal = ({isModalOpen, toggleModal, onRemoveFromCart, productIDToRemove}) => {
  return (
    <Modal
      className="deleteConfirmationModal"
      isOpen={isModalOpen}
      toggle={toggleModal}
    >
      <div className="modalHeader">Remove from cart</div>
      <div>
        Are you sure to remove this item from the cart? This action can't be
        undone
      </div>
      <div className="buttons">
        <button onClick={() => onRemoveFromCart(productIDToRemove)}>
          Yes, remove
        </button>
        <button onClick={() => toggleModal()}>Cancel</button>
      </div>
    </Modal>
  );
};

export default DeleteItemModal;

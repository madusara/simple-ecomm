import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { Container } from "reactstrap";
import shoppingCart from "../../images/icons/shopping-cart.png";
import styles from "./index.module.scss";

const AppNavbar = ({ itemsCount }) => {
  return (
    <div className={styles.appNavbar}>
      <Container className={styles.container}>
        <Link className={styles.leftText} to="/">
          Mobile <span>Phones</span>
        </Link>
        <Link className={styles.rightButton} to="/cart">
          <img src={shoppingCart} alt="" />({itemsCount})
        </Link>
      </Container>
    </div>
  );
};

export default AppNavbar;

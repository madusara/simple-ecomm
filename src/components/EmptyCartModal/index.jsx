import React from "react";
import { Modal } from "reactstrap";

// this modal is used to prompt the user to confirm if they really want to empty the cart

const EmptyCartModal = ({isModalOpen, toggleModal, handleEmptyCart}) => {
  return (
    <Modal
      className="deleteConfirmationModal"
      isOpen={isModalOpen}
      toggle={toggleModal}
    >
      <div className="modalHeader">Empty the cart</div>
      <div>
        Are you sure to empty the cart completely? This action can't be
        undone
      </div>
      <div className="buttons">
        <button onClick={() => handleEmptyCart()}>
          Yes, Empty the cart
        </button>
        <button onClick={() => toggleModal()}>Cancel</button>
      </div>
    </Modal>
  );
};

export default EmptyCartModal;

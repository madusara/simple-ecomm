import React, { useState, useEffect } from "react";
import { toast } from "react-toastify";
import { Container } from "reactstrap";
import { Link } from "react-router-dom";

import styles from "./index.module.scss";
import "./index.scss";
import trashCan from "./../../images/icons/trash-can.png";
import OrderSummary from "../../components/OrderSummary";
import DeleteItemModal from "../../components/DeleteItemModal";
import EmptyCartModal from "../../components/EmptyCartModal";
import AppNavbar from "../../components/AppNavbar";

const Cart = ({ productsArray }) => {
  const [myCartState, setMyCartState] = useState(
    JSON.parse(localStorage.getItem("myCart")) || []
  );
  const [isDeleteItemModalOpen, setDeleteItemModalOpen] = useState(false);
  const [isEmptyCartModalOpen, setEmptyCartModalOpen] = useState(false);

  const [productIDToRemove, setProductIDToRemove] = useState("");
  const [subTotal, setSubTotal] = useState(0);
  const [itemsCount, setItemsCount] = useState(0);
  const [tax, setTax] = useState(0);

  useEffect(() => {
    // calculating total number of items and the subTotal
    let noOfItems = 0;
    let totalAmount = 0;
    myCartState.forEach((item, index) => {
      totalAmount = totalAmount + item.price * item.count;
      noOfItems = noOfItems + item.count;
    });
    setItemsCount(noOfItems);
    setSubTotal(totalAmount);
  }, [myCartState]);

  useEffect(() => {
    setTax(+(subTotal * 0.0123).toFixed(2));
  }, [subTotal]);

  const openDeleteItemModal = (id) => {
    // save the product id in the component state
    setProductIDToRemove(id);
    setDeleteItemModalOpen(true);
  };

  const onRemoveFromCart = () => {
    toggleDeleteItemModal();
    handleRemoveFromCart(productIDToRemove);
  };

  const onEmptyCart = () => {
    window.localStorage.removeItem("myCart");
    setMyCartState([]);
  };

  const toggleDeleteItemModal = () => {
    setDeleteItemModalOpen(!isDeleteItemModalOpen);
  };

  const toggleEmptyCartModal = () => {
    setEmptyCartModalOpen(!isEmptyCartModalOpen);
  };

  const handleModifyCount = (functionality, cid) => {
    const cartCopy = JSON.parse(localStorage.getItem("myCart")) || [];
    let cartItemIndex = cartCopy.findIndex((product) => {
      return product.productID === cid;
    });

    if (cartItemIndex > -1) {
      if (functionality === "INCREMENT") {
        // we need to know if the stocks are available before letting the user to increment a particular product
        // for that I've identified the respective product entry in the productsArray
        let productInTheProductsArray = productsArray.filter((product) => {
          return product.id === cid;
        });

        // now as I know the product entry, I'll check it's stock
        const availableAmountInStock =
          productInTheProductsArray.length > 0 &&
          productInTheProductsArray[0].availability;

        // if the stock is enough for the user to increment the count, it will allow. Else we have to show an error/toast
        if (cartCopy[cartItemIndex].count < availableAmountInStock)
          cartCopy[cartItemIndex].count = cartCopy[cartItemIndex].count + 1;
        else toast.error("Error: No more stocks available");
      } else {
        // We are not letting the user to keep decrementing until 0 occurs.
        // If they want to delete the entire product from the cartCopy, we've given the delete option separately
        if (cartCopy[cartItemIndex].count === 1)
          toast.error(
            "Error: Can't delete the only product. Please click the trash icon to delete"
          );
        else cartCopy[cartItemIndex].count = cartCopy[cartItemIndex].count - 1;
      }
      setMyCartState([...cartCopy]);
      window.localStorage.setItem("myCart", JSON.stringify([...cartCopy]));
    }
  };

  const handleRemoveFromCart = (cid) => {
    // replicating the state array of products
    const cartCopy = [...myCartState];

    // finding the index of the product in the cart
    let cartItemIndex = cartCopy.findIndex((product) => {
      return product.productID === cid;
    });

    // deletion
    cartCopy.splice(cartItemIndex, 1);
    toast.success("Product was removed from the cart successfully");
    setMyCartState([...cartCopy]);
    window.localStorage.setItem("myCart", JSON.stringify([...cartCopy]));
  };

  // we will show a banner if the cart is empty
  if (myCartState.length === 0)
    return (
      <div className={styles.emptyCartMessageWrapper}>
        <div>
          <h3>Cart is Empty</h3>
          <div>
            Your cart is empty. Please add products to you cart from the{" "}
            <Link to="/">products</Link> page
          </div>
        </div>
      </div>
    );
  // if products are added to the cart, we will display the order summary
  else
    return (
      <Container>
        <AppNavbar itemsCount={itemsCount} />
        <div className={styles.cartPage}>
          <h2>Cart</h2>
          <div className={styles.contentWrap}>
            <div className={styles.listOfDetailedItems}>
              {myCartState.length > 0 &&
                myCartState.map((each, index) => {
                  return (
                    <div className={styles.eachItem} key={index}>
                      <div className={styles.leftsideOfFlex}>
                        <img src={each.imgURL} alt="" />
                        <div>
                          {each.make} {each.name}
                        </div>
                        <div className={styles.priceStyle}>
                          &nbsp;| ₹{each.price}
                        </div>
                      </div>
                      <div className={styles.rightsideOfFlex}>
                        <div>{each.count}</div>
                        <button
                          onClick={() =>
                            handleModifyCount("DECREMENT", each.productID)
                          }
                        >
                          -
                        </button>
                        <button
                          onClick={() =>
                            handleModifyCount("INCREMENT", each.productID)
                          }
                        >
                          +
                        </button>
                        <button
                          onClick={() => openDeleteItemModal(each.productID)}
                        >
                          <img src={trashCan} alt="" />
                        </button>
                      </div>
                    </div>
                  );
                })}
              <button onClick={() => toggleEmptyCartModal()}>Empty cart</button>
            </div>
            <OrderSummary
              itemsCount={itemsCount}
              subTotal={subTotal}
              tax={tax}
            />
          </div>
          <DeleteItemModal
            isModalOpen={isDeleteItemModalOpen}
            toggleModal={toggleDeleteItemModal}
            onRemoveFromCart={onRemoveFromCart}
            productIDToRemove={productIDToRemove}
          />
          <EmptyCartModal
            isModalOpen={isEmptyCartModalOpen}
            toggleModal={toggleEmptyCartModal}
            handleEmptyCart={onEmptyCart}
          />
        </div>
      </Container>
    );
};

export default Cart;

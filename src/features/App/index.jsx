import React, { useState, useEffect } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Routes, Route } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import axios from "axios";
import { Container } from "reactstrap";
import Cart from "../Cart";
import Products from "../Products";
import "./index.scss";

const App = () => {
  const [productsArray, setProductsArray] = useState([]);
  const [myCartState, setMyCartState] = useState(
    JSON.parse(localStorage.getItem("myCart")) || []
  );
  const [itemsCount, setItemsCount] = useState(0);

  useEffect(() => {
    // mock products json file is inside the public folder
    axios.get("./products.json").then((response) => {
      setProductsArray(response.data);
    });
  }, []);

  useEffect(() => {
    // calculating total number of items to display in the nav bar
    let noOfItems = 0;
    myCartState.forEach((item, index) => {
      noOfItems = noOfItems + item.count;
    });
    setItemsCount(noOfItems);
  }, [myCartState]);

  // this runs when products.jsx loads (via its useEffect)
  // this grabs the cart state from the local storage
  const calculateNumberOfItems = () => {
    setMyCartState(JSON.parse(localStorage.getItem("myCart")) || []);
  };

  const handleAdd = async (cid) => {
    // duplicating myCart array
    const cartCopy = JSON.parse(localStorage.getItem("myCart")) || [];

    // Checking if I have already added the product to the Cart.
    let foundIndex = cartCopy.findIndex((product) => {
      return product.productID === cid;
    });

    // if already exists in the cart array, increment it's count.
    if (foundIndex > -1) {
      cartCopy[foundIndex].count = cartCopy[foundIndex].count + 1;

      window.localStorage.setItem("myCart", JSON.stringify(cartCopy));
      setMyCartState([...cartCopy]);
      toast.success("Product was added to the cart successfully!");

      // if the item doesn't exist in the cart, create a new entry
    } else {
      let foundInProducts = productsArray.filter((product) => {
        return product.id === cid;
      });
      const newItem = {
        productID: cid,
        count: 1,
        name: foundInProducts[0]?.name,
        make: foundInProducts[0]?.make,
        imgURL: foundInProducts[0]?.imgURL,
        price: foundInProducts[0]?.price,
      };

      // Append the newItem to the old cart array
      window.localStorage.setItem(
        "myCart",
        JSON.stringify([...cartCopy, newItem])
      );
      setMyCartState([...cartCopy, newItem]);
      toast.success("Product was added to the cart successfully!");
    }
  };

  return (
    <div>
      {/* we are using react-toastify to show toasts on important events/errors etc */}
      <ToastContainer autoClose={25000} hideProgressBar={true} />
      <Routes>
        <Route
          path="/"
          element={
            <Products
              handleAdd={handleAdd}
              productsArray={productsArray}
              itemsCount={itemsCount}
              calculateNumberOfItems={calculateNumberOfItems}
            />
          }
        />
        <Route path="/cart" element={<Cart productsArray={productsArray} />} />
      </Routes>
    </div>
  );
};

export default App;

import React, { useEffect, useState } from "react";
import styles from "./index.module.scss";
import ProductInList from "../../components/ProductInList";
import AppNavbar from "../../components/AppNavbar"
import { Container } from "reactstrap";

const Products = ({ productsArray, handleAdd, itemsCount, calculateNumberOfItems }) => {

  useEffect(() => {
    calculateNumberOfItems()
  }, [])
  
  
  return (
    <Container>
      <AppNavbar itemsCount={itemsCount} />
      <div className={styles.productsPage}>
        <h2>Products</h2>
        <div className={styles.productsWrap}>
          {productsArray &&
            productsArray.map((product, index) => (
              <ProductInList
                data={product}
                key={index}
                id={index}
                handleAdd={handleAdd}
              />
            ))}
        </div>
      </div>
    </Container>
  );
};

export default Products;
